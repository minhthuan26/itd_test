import static org.junit.jupiter.api.Assertions.*;

class MyBigNumberTest {

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    void sum() {
        String expectedResult = "10000000000000000000000000000000000000000000000000000000000000";
        MyBigNumber myBigNumber = new MyBigNumber();
        String result = myBigNumber.sum("1", "9999999999999999999999999999999999999999999999999999999999999");
        assertEquals(expectedResult, result);
    }
}