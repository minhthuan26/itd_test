public class MyBigNumber {
    String num1, num2;

    public MyBigNumber() {

    }

    public MyBigNumber(String num1, String num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public String sum(String num1, String num2){

        String result= "";
        int sum = 0;
        String upperLine;
        String lowerLine;

        if(num1.length() >= num2.length()){
            upperLine = num1;
            lowerLine = num2;
        }
        else {
            upperLine = num2;
            lowerLine = num1;
        }

        int maxLength = upperLine.length();
        int minLength = lowerLine.length();

        if(minLength == maxLength){
            for(int i=maxLength-1; i>=0; i--){
                int upperLineNumber = Integer.parseInt(upperLine.substring(i,i+1));
                int lowerLineNumer = Integer.parseInt(lowerLine.substring(i,i+1));

                sum += upperLineNumber + lowerLineNumer;

                if (sum >= 10 && maxLength > 1) {
                    result = sum % 10 + result;
                    sum = 1;
                } else {
                    result = sum + result;
                    sum = 0;
                }
            }
        } else {
            for(int i=maxLength-1, j=minLength-1; i>=0; i--, j--){

                if(j >= 0){
                    int upperLineNumber = Integer.parseInt(upperLine.substring(i, i+1));
                    int lowerLineNumer = Integer.parseInt(lowerLine.substring(j, j+1));
                    sum += upperLineNumber + lowerLineNumer;

                    if (sum >= 10) {
                        result = sum % 10 + result;
                        sum = 1;
                    } else {
                        result = sum + result;
                        sum = 0;
                    }
                } else {
                    if(sum != 0) {
                        int upperNumer = Integer.parseInt(upperLine.substring(i, i + 1));
                        sum += upperNumer;

                        if (sum >= 10) {
                            result = sum % 10 + result;
                            sum = 1;
                        } else {
                            result = sum + result;
                            sum = 0;
                        }
                    } else {
                        result = upperLine.substring(0, i+1) + result;
                        break;
                    }
                }
            }
        }

        if(sum == 1) {
            result = sum + result;
        }

        System.out.println(num1 + " + " + num2 + " = " + result);
        return result;
    }

}
